<?php
	error_reporting(E_ALL ^ E_NOTICE);

	$connection = null;

    use Neoxygen\NeoClient\ClientBuilder;
	require 'vendor/autoload.php';

	// Using login and password 
	$connection = ClientBuilder::create()
	  ->addConnection('default', 'http', 'localhost', 7474, true, 'neo4j2', 'neo4j2')
	  ->setAutoFormatResponse(true)
	  ->build();
?>
